﻿#include <iostream>
#include "Parser.hpp"

auto main(int numArgs, char** args) -> int
{
	std::unordered_map<std::string, int> cmdMap =
	{
		//compile
		{"-c", 1},
		{"--compile",1},
		//run
		{"-r",2},
		{"--run",2},
		//help
		{"-h",3},
		{"--help",3},
		//convert to C
		{"-to_c",4}
	};

	bool invalid = true;
	for (int i = 0; i < numArgs; i++)
	{
		std::string t(args[i]);
	
		std::transform(t.begin(), t.end(), t.begin(), [](char c) -> char
			{
				return char(tolower(char(c)));
			}
		);

		switch (cmdMap[t])
		{
			case 1:
				invalid = false;
				ParseFile(args[i]);
				break;
			case 2:
				invalid = false;
				ParseFile(args[i]);
				break;
			case 3:
				std::cout <<
					"Pseudocode interpreter : command line arguemnts\r\n"
					"-c, --compile : Compiles a source into a byte code. Arguments \r\n"
					"\t <path> : Path to source file\r\n"
					
					"-r, --run : Runs a byte-code file, or compiles and runs a source file. Arguments:\r\n"
					"\t <path> : Path to source file, or to a compiled source containing byte code \r\n"
					
					"-to_c : Converts the code to a C source file"

					"-h, --help : Displays this help message.\r\n";
				invalid = false;
				break;
			case 4:
			{
				break;
			}
			default:
				if (!invalid)
					invalid = true;
				break;
		}
	}
	if (invalid)
	{
		std::cerr << "pseudocode_interpreter [-h] (-r <path>) (-c <path>) \r\n";
		return -1; //invalid arguments.
	}
}
#include "Parser.hpp"
#include <exception>
#include <ranges>
#include "VM.hpp"

auto separateToken (const std::string &token, std::vector<std::string>& tokens) -> void //recursive lambda
{
	std::string chrs = "=+-*.():;&!%<>[]{}\t";

	for(auto c : chrs)
	{
		auto findResult = token.find(c);
		if(findResult != std::string::npos)
		{
			separateToken(token.substr(0, findResult));
			tokens.push_back(std::string("") + c);
			separateToken(token.substr(findResult + 1));
		}
		else tokens.push_back(token);
	}
};

std::ifstream openFile(const std::filesystem::path &path)
{
	//check if file exists
	if(!std::filesystem::exists(path))
		throw std::exception((path.string() + "doesn't exist!").c_str());

	std::ifstream f(path); // open a file stream

	//check if the filestream was sucessfully created
	if(f.bad() || f.fail())
		throw std::exception(("Failed to open " + path.string()).c_str());

}

std::unordered_map<std::string, ExpressionType> keywords =
{
	{"fn",ExpressionType::Function},
	{"N", ExpressionType::Natural},
	{"natural", ExpressionType::Natural},
	{"Z", ExpressionType::Integer},
	{"intreg", ExpressionType::Integer},
	{"integer",ExpressionType::Integer},
	{"int", ExpressionType::Integer},
	{"real", ExpressionType::Real},
	{"complex", ExpressionType::Complex},
	{"struct", ExpressionType::Struct},
	{"variant", ExpressionType::Variant},
	{"daca", ExpressionType::If},
	{"if", ExpressionType::If},
	{"cat_timp", ExpressionType::While},
	{"while", ExpressionType::While},
	{":", ExpressionType::DDot},
	{"=", ExpressionType::Equal},
	{";", ExpressionType::End},
	{",", ExpressionType::Comma},
	{"==", ExpressionType::BoolEquals},
	{"!", ExpressionType::Negation},
	{"not", ExpressionType::Negation},
	{"+", ExpressionType::Plus},
	{"-", ExpressionType::Minus},
	{"*", ExpressionType::Multiplication},
	{"/", ExpressionType::Division},
	{"mod", ExpressionType::Mod},
	{"(", ExpressionType::ParanthesisOpen},
	{")", ExpressionType::ParanthesisClose},
	{"and", ExpressionType::And},
	{"&",ExpressionType::BitwiseAnd},
	{"or", ExpressionType::Or},
	{"|", ExpressionType::BitwiseOr},
	{"ret", ExpressionType::Return },
	{"->", ExpressionType::FunctionReturnTypeDef},
	{"operator", ExpressionType::Operator},
};

bool isNum(char c)
{
	return c >= '0' && c <= '9';
}

VariableType determineVariableType(const std::string &&expr)
{
	
}

void ParseFile(const std::filesystem::path& path)
{
	auto f = openFile(path);

	//read each line
	while (!f.eof())
	{
		std::string line;
		std::getline(f, line);

		std::vector<std::string> tokens;

		//same as strtok(...)
		auto lsplt = std::views::split(line, " ");
		for(auto it = lsplt.begin(); it != lsplt.end(); it++)
		{
			auto currentToken = (*it);
			std::string str(std::string_view(currentToken.begin(), currentToken.end()));
			
			//lexical analysis.
			separateToken(str, tokens);
		}

		std::unordered_map<std::string, std::pair<int, VariableType>> variables; //name = [ID, type]

		auto evaluateAST = [&](int begin, int end)
		{

		};
		std::vector<std::variant<std::string, ExpressionType>> code;
		for(auto &token : tokens)
		{
			if(keywords[token] != ExpressionType::Unknown)
				code.push_back(keywords[token]);
			else code.push_back(token);

			code.push_back(ExpressionType::End);
		}

		//predefined functions:
		variables[" "] = {0, VariableType::NonExistent};
		variables["print"] = {1, VariableType::Function};

		//bytecode generation


		std::vector<ExInstruction> instructions;
		
		for(int i = 0, j = 0; i < code.size();)
		{
			std::visit(
				[i, &j, &instructions, &variables, &code](auto &&e) 
			{
				using T = std::decay_t<decltype(e)>;

				if constexpr(std::is_same_v<T, std::string>)
				{
					if(j == 0)
					{
						//Case 1: var_name = value (;)
						
						int varID = variables[std::string(e)].first;
						VariableType varType = variables[std::string(e)].second;

						if(varID != 0) //if variable already exists
						{
							instructions.push_back(ExInstruction(Instruction::Assignation, {}));
						}
						else
						{

						}
					}
				}
				if constexpr(std::is_same_v<T, ExpressionType>)
				{
					//function definition
					if(e == ExpressionType::Function)
					{
						std::string fName = std::get<std::string>(code[i + 1]);

						if(std::get<ExpressionType>(code[i + 2]) == ExpressionType::ParanthesisOpen) // (
						{
							for(int k = 0; std::get<ExpressionType>(code[i + 3 + k]) != ExpressionType::ParanthesisClose; )
							{

							}
						}
					}

					//end statement
					if(e == ExpressionType::End) j = 0;
				}
			}
			, code[i]);
		}

	}

	f.close();
}

void ConvertToC(const std::filesystem::path &path)
{
	auto f = openFile(path);

	while(!f.eof())
	{

	}
}

#pragma once

#include <string>
#include <algorithm>
#include <regex>
#include <unordered_map>
#include <filesystem>
#include <exception>
#include <fstream>
#include <variant>
#include <typeindex>

#include "ASTree/astree.hpp"
void ParseFile(const std::filesystem::path& path);

void ConvertToC(const std::filesystem::path &path);

enum class VariableType
{
	NonExistent = 0,
	Void,
	Natural,
	Integer,
	Real,
	Complex,
	Struct,
	Variant,
	Function,
	Method,
	String,
};

enum class ExpressionType
{
	Unknown = 0,
	Function, // fn
	Natural, // N
	Integer, // Z
	Real, // R
	Complex, // C
	Struct, // struct
	Variant, //union
	If, // daca
	While, //cat_timp; while
	DDot, // :
	Equal, // =
	End, // ;
	Comma, // ,
	BoolEquals, // == 
	Negation, // !, NOT
	Plus, // +
	Minus, // -
	Multiplication, // *
	Division, // /
	Mod, //mod (remainder), %
	ParanthesisOpen, //(
	ParanthesisClose, //)
	And, //AND
	BitwiseAnd, //|
	Or,  //OR
	BitwiseOr, //&
	Return, //ret
	FunctionReturnTypeDef, //->
	Operator, //
}; 
#include "VM.hpp"

ExInstruction::ExInstruction(Instruction i, VariableList args):
	Instr(i),
	Arguments(args)
{
}

std::optional<Variable> ExInstruction::Execute(VM& Context)
{
	auto instr = this->Instr;
	auto variables = this->Arguments;

	switch(instr)
	{
		// +
		case Instruction::Add_N:
			return std::make_pair(std::any_cast<unsigned>(variables[0]) + std::any_cast<unsigned>(variables[1]), VariableType::Natural);
		case Instruction::Add_Z:
			return std::make_pair(std::any_cast<int>(variables[0]) + std::any_cast<int>(variables[1]), VariableType::Integer);
		case Instruction::Add_R:
			return std::make_pair(std::any_cast<float>(variables[0]) + std::any_cast<float>(variables[1]), VariableType::Real);
		case Instruction::Add_C:
			return std::make_pair(std::any_cast<std::complex<float>>(variables[0]) + std::any_cast<std::complex<float>>(variables[1]), VariableType::Complex);
			// -
		case Instruction::Substract_N:
			return std::make_pair(std::any_cast<unsigned>(variables[0]) - std::any_cast<unsigned>(variables[1]), VariableType::Natural);
		case Instruction::Substract_Z:
			return std::make_pair(std::any_cast<int>(variables[0]) - std::any_cast<int>(variables[1]), VariableType::Integer);
		case Instruction::Substract_R:
			return std::make_pair(std::any_cast<float>(variables[0]) - std::any_cast<float>(variables[1]), VariableType::Real);
		case Instruction::Substract_C:
			return std::make_pair(std::any_cast<std::complex<float>>(variables[0]) - std::any_cast<std::complex<float>>(variables[1]), VariableType::Complex);
			// *
		case Instruction::Multiply_N:
			return std::make_pair(std::any_cast<unsigned>(variables[0]) * std::any_cast<unsigned>(variables[1]), VariableType::Natural);
		case Instruction::Multiply_Z:
			return std::make_pair(std::any_cast<int>(variables[0]) * std::any_cast<int>(variables[1]), VariableType::Integer);
		case Instruction::Multiply_R:
			return std::make_pair(std::any_cast<float>(variables[0]) * std::any_cast<float>(variables[1]), VariableType::Real);
		case Instruction::Multiply_C:
			return std::make_pair(std::any_cast<std::complex<float>>(variables[0]) * std::any_cast<std::complex<float>>(variables[1]), VariableType::Complex);
			// division
		case Instruction::Divide_N:
			return std::make_pair(std::any_cast<unsigned>(variables[0]) / std::any_cast<unsigned>(variables[1]), VariableType::Natural);
		case Instruction::Divide_Z:
			return std::make_pair(std::any_cast<int>(variables[0]) / std::any_cast<int>(variables[1]), VariableType::Integer);
		case Instruction::Divide_R:
			return std::make_pair(std::any_cast<float>(variables[0]) / std::any_cast<float>(variables[1]), VariableType::Real);
		case Instruction::Divide_C:
			return std::make_pair(std::any_cast<std::complex<float>>(variables[0]) / std::any_cast<std::complex<float>>(variables[1]), VariableType::Complex);
			// mod
		case Instruction::Modulo_N:
			return std::make_pair(std::any_cast<unsigned>(variables[0]) % std::any_cast<unsigned>(variables[1]), VariableType::Natural);
		case Instruction::Modulo_Z:
			return std::make_pair(std::any_cast<int>(variables[0]) % std::any_cast<int>(variables[1]), VariableType::Integer);
			// f(...)
		case Instruction::Call_Function:
		{
			auto f = std::any_cast<int>(variables[0]);
			Context.InstructionIndex = f;

			auto numVars = std::any_cast<unsigned>(variables[1]);

			for(int i = 0; i < numVars; i++)
			{
				Context.Locals.push_back(variables[i + 2]);
			}

		}
		case Instruction::Jump_if_true:
		{
			if(Context._if && Context._cFlag)
			{
				Context.CurrentBlock = std::any_cast<int>(variables[0]);
				Context._if = Context._cFlag = false;
			}
			return {};
		}
		case Instruction::UnconditionalJump:
		{
			Context.CurrentBlock = std::any_cast<int>(variables[0]);
			return {};
		}
		case Instruction::Equals:
			return std::make_pair(variables[0] == variables[1], VariableType::Natural);
		case Instruction::NotEquals:
			return std::make_pair(variables[0] != variables[1], VariableType::Natural);
	}
}

void VM::Start(std::vector<ExInstruction> &p)
{
	for(; InstructionIndex < p.size(); )
	{
		p[InstructionIndex++].Execute(*this);
	}
}

#pragma once
#include <any>
#include <span>
#include <optional>
#include <complex>
#include "Parser.hpp"
#include <stack>

enum class Instruction
{
	/// <summary>
	/// Do nothing.
	/// </summary>
	NOP,
	//Mathematical opcodes

	/// <summary>
	/// Adds two natural variables.
	/// </summary>
	Add_N,
	/// <summary>
	/// Adds two integers.
	/// </summary>
	Add_Z,
	/// <summary>
	/// Adds two real numbers.
	/// </summary>
	Add_R,
	/// <summary>
	/// Adds two complex numbers.
	/// </summary>
	Add_C,

	Substract_N,
	Substract_Z,
	Substract_R,
	Substract_C,

	Multiply_N,
	Multiply_Z,
	Multiply_R,
	Multiply_C,

	Divide_N,
	Divide_Z,
	Divide_R,
	Divide_C,

	Modulo_N,
	Modulo_Z,

	//jump opcodes
	Call_Function,
	Jump_if_true,
	Jump_if_false,
	UnconditionalJump,
	Ret,

	//logic
	Equals,
	NotEquals,
	Negate,
	And,
	Or,

	//byte ops
	BitwiseAnd,
	BitwizeOr,

	//variables
	Assignation,
	CreateVariable,

	//others
	Load_Library,
	Call_External_Function,
	Print,
};

struct ExInstruction
{
	Instruction Instr;
	VariableList Arguments;

	ExInstruction(Instruction i, VariableList args);

	std::optional<Variable> Execute(VM &Context);
};

/// <summary>
/// Simulates a CPU. Executes a instruction one by one.
/// </summary>
struct VM
{
	/// <summary>
	/// Global variables.
	/// </summary>
	std::vector<Variable> Globals;
	/// <summary>
	/// Local variables.
	/// </summary>
	std::vector<Variable> Locals;

	std::stack<int> Callstack;

	int InstructionIndex;
	int CurrentBlock;
	void Start(std::vector< ExInstruction> &p );

	std::vector<ExInstruction> program;

	int _if;
	int _cFlag;
};


using Variable = std::pair<std::any, VariableType>;
using VariableList = std::span<Variable>;